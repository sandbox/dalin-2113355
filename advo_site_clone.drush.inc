<?php

/**
 * @file
 * Clones an existing site on an Advo dev machine.
 *
 * WARNING: This code is woefully insecure.  Shell commands are not properly
 * escaped. It is assumed that if you are running this code you already have
 * CLI access.
 */

/**
 * Implementation of hook_drush_command().
 */
function advo_site_clone_drush_command() {
  $items = array();

  // The 'make-sync' command
  $items['advo-site-clone'] = array(
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'description' => "Clones an existing site on an Advo dev machine.",
    'options' => array(
      'destination-domain' => 'Required. The domain of the destination site.',
      'destination-db-name' => 'Required. The name of the DB that will be created for the new site.',
      'destination-path' => 'Where in the filesystem should the new site be created.  The default is one directory higher than the current webroot.',
      'git-branch' => 'Name of a new branch to create in Git. Optional.',
      'db-su' => 'Account to use when creating a new database. Optional.',
      'db-su-pw' => 'Password for the "db-su" account. Optional.',
      'force' => 'Continue even if --destination-path or --destination-db-name already exist.',
      'continue-on-error' => 'Continue later steps even if an earlier one fails.',
    ),
    'examples' => array(
      'drush advo-site-clone -l advobase.dev.advomatic.com --destination-domain=mybranch.advobase.dev.advomatic.com --destination-db-name=advobase_mybranch' => 'Creates a new site at the domain mybranch.advobase.dev.advomatic.com.  The DB from advobase.dev.advomatic.com will be coppied to it.',
    ),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function advo_site_clone_drush_help($section) {
  switch ($section) {
    case 'drush:advo-site-clone':
      return dt("Clones an existing site on an Advo dev machine.");
  }
}

/**
 * Implements drush_COMMAND_validate().
 *
 * @todo break this up into about 20 sub-functions.
 */
function drush_advo_site_clone_validate() {
  drush_print('---VALIDATION---');
  
  // Check for the bare minimum.
  foreach (array('destination-domain', 'destination-db-name') as $required) {
    if (!drush_get_option($required)) {
      drush_set_error('missing_required_option', dt('The --!option option is required.  See `drush help`.', array('!option' => $required)));
    }
  }
  if (drush_get_error()) {
    return;
  }

  // Test all levels of the destination path to ensure that they exist, or are
  // createable?
  $destination_path = _advo_site_clone_get_destination_path();
  $destination_array = explode('/', $destination_path);
  foreach ($destination_array as $i => $part) {
    // Skip the root.
    if (!$i) {
      continue;
    }
    $part_up_one_path = implode('/', array_slice($destination_array, 0, $i));
    $part_path = $part_up_one_path . '/' . $part;
    $message_args = array(
      '!destination_path' => $destination_path,
      '!part_path' => $part_path,
      '!part_up_one_path' => $part_up_one_path,
    );
    if (!file_exists($part_path)) {
      if (!is_writable($part_up_one_path)) {
        drush_set_error('destination_path_not_writable', dt('--destination-path is !destination_path but !part_up_one_path is not writable', $message_args));
        break;
      }
      elseif (!mkdir($part_path, 0775)) {
        drush_set_error('destination_path_not_writable', dt('--destination-path is !destination_path but could not create !part_path.  Check permissions of !part_up_one_path', $message_args));
        break;
      }
      else {
        drush_log(dt('!part_path created.', $message_args));
      }
    }
    else {
      drush_log(dt('!part_path exists.', $message_args));
    }
  }

  // Is the domain valid?
  if (!valid_url(drush_get_option('destination-domain'))) {
    drush_set_error('invalid_domain', dt('--destination-domain is not valid according to valid_url().'));
  }
  if (strpos(drush_get_option('destination-domain'), '_') !== FALSE) {
    drush_set_error('invalid_domain', dt('--destination-domain may not contain underscores.'));
  }

  // Is the DB name valid?
  if (!preg_match('!^[a-zA-A][a-zA-Z_0-9]{2,15}$!', drush_get_option('destination-db-name'))) {
    drush_set_error('invalid_db_name', dt('--destination-db-name must only include alphanumerics and underscores and be 16 characters or less.'));
  }

  // Is there a source site?
  if (!drush_get_context('DRUSH_DRUPAL_SITE_ROOT')) {
    drush_set_error('no_source_site', dt('No source site could be determined.  Use --url=[sitename].'));
  }

  // Test to see if destination site already exists.
  if (file_exists(_advo_site_clone_get_destination_path() . '/index.php')) {
    $error_message = dt('The destination path already exists.');
    if (drush_get_option('force')) {
      drush_log($error_message . ' It will be overwritten.', 'warning');
    }
    else {
      drush_set_error('destination_path_exists', $error_message . ' If you are sure that you want to continue (possibly due to a minor error on a previous attempt), use --force.');
    }
  }

  // Does the destination domain look like a derivative of the current domain?
  $source_domain_parts = explode('.', str_replace('sites/', '', drush_get_context('DRUSH_DRUPAL_SITE_ROOT')));
  $dev_part = array_search('dev', $source_domain_parts);
  if ($dev_part !== FALSE && isset($source_domain_parts[$dev_part - 1])) {
    $source_domain_parts = array_slice($source_domain_parts, $dev_part - 1);
  }
  $source_domain = implode('.', $source_domain_parts);
  if (!preg_match('!' . preg_quote($source_domain) . '$!', drush_get_option('destination-domain'))) {
    drush_log(dt('Destination domain is usually in the form of [something].!source_domain.', array('!source_domain' => $source_domain)), 'warning');
  }

  // Test if MySQL user can write to the DB.
  $creds = drush_get_context('DRUSH_DB_CREDENTIALS');
  $db_su_creds = _advo_site_clone_get_db_credentials();
  if ($db_su_creds['db-su']) {
    $creds['user'] = $db_su_creds['db-su'];
    $creds['pass'] = $db_su_creds['db-su-pw'];
  }
  $prefix = function($key) {
    return "!$key";
  };
  $keys = array_map($prefix, array_keys($creds));
  $error_args = array_combine($keys, $creds);
  $connection = new mysqli($creds['host'], $creds['user'], $creds['pass'], $creds['name'], (int) $creds['port']);
  if ($connection && !$connection->connect_error) {
    $error_message = dt('Cannot create new DB with !user / !pass.', $error_args);
    try {
      // Use a random name, just in case for some reason the DB gets created, 
      // but not dropped.
      $test_db = 'test' . mt_rand(10000, 99999);
      if ($connection->query("CREATE DATABASE IF NOT EXISTS $test_db;")) {
        $connection->query("DROP DATABASE IF EXISTS $test_db;");
        drush_log('DB can be created.');


      }
      else {
        // Not sure that we actually need this one, I think it'll just throw.
        drush_set_error('db_su_privileges', $error_message);
      }
    }
    catch (Exception $e) {
      drush_set_error('db_su_privileges', $error_message);
    }

    // Test to see if destination DB already exists.
    $error_message = dt('The destination DB already exists.');
    $destination_db = $connection->real_escape_string(drush_get_option('destination-db-name'));
    $result = $connection->query("SHOW DATABASES LIKE '$destination_db'");
    if (!$result) {
      drush_print_r('error: ' . $connection->sqlstate . $connection->error);
    }
    elseif ($result->num_rows) {
      if (drush_get_option('force')) {
        drush_log($error_message . ' It will be overwritten.', 'warning');
      }
      else {
        drush_set_error('destination_db_exists', $error_message . ' If you are sure that you want to continue and completely overwrite the database, use --force.');
      }
    }
    $connection->close();
  }
  else {
    drush_set_error('db_connect', dt('Cannot connect to DB with !user / !pass. ', $error_args));
  }

  // Does Git exist?
  // Even if we don't create a branch we need it to ignore the new sites
  // directory.
  $git_exists = drush_shell_exec("which git");
  if (!$git_exists) {
    drush_set_error('no_git', dt('Git must be installed to use this command.'));
  }

  $git_branch = drush_get_option('git-branch');
  if ($git_branch && $git_exists) {
    // Validate Git branch name.
    // Not quite sure what to allow here.  For now this:
    if (!preg_match('!^[a-zA-Z_0-9-]{2,16}$!', $git_branch)) {
      drush_set_error('invalid_git_branch_name', dt('--git-branch must only include alphanumerics, underscores and hyphens and be 16 characters or less.'));
    }

    // Make sure our info is up to date.
    $exec = 'git fetch origin';
    drush_shell_exec($exec);

    // Confirm that the Git branch doesn't already exist.
    $exec = 'git branch -la';
    drush_shell_exec($exec);
    foreach (drush_shell_exec_output() as $branch) {
      foreach (array('*', 'remotes/', 'origin/') as $replacement) {
        $branch = str_replace($replacement, '', $branch);
      }
      $branch = trim(preg_replace('!->.+!', '', $branch));
      if ($branch == $git_branch) {
        drush_set_option('git-branch-exists', TRUE);
        drush_log(dt('The Git branch already exists. It will be checked out.'), 'warning');
      }
    }

    // Do not continue if the repo is not clean.
    $exec = 'git status --porcelain';
    if (drush_shell_exec($exec) && drush_shell_exec_output()) {
      if (!drush_get_option('force')) {
        drush_set_error('git_unclean', dt('Git is not in a clean state.  Run `git status` for more info or add --force.'));
      }
      elseif ($git_branch) {
        drush_log(dt('Creating a new branch when the git repo is not clean might be a bad idea.  Run `git status` for more info.'), 'warning');
      }
      else {
        drush_log(dt('Git is not in a clean state, but continuing anyway due to --force.  Run `git status` for more info.'), 'warning');
      }
    }
  }

  $exec = 'git rev-list HEAD --not --remotes';
  if ($git_exists && drush_shell_exec($exec) && drush_shell_exec_output()) {
    drush_set_error('git_ahead', dt('Git is ahead of the remote branch.  See `git status` for more info.'));
  }

  if (!drush_shell_exec("which rsync")) {
    drush_set_error('no_rsync', dt('Rsync must be installed to use this command.'));
  }

  // Check that Apache is setup.
  if (!drush_shell_exec("which fgrep")) {
    drush_set_error('no_fgrep', dt('fgrep must be installed to use this command.'));
    $no_fgrep = TRUE;
  }
  if (!drush_shell_exec("which wget")) {
    drush_set_error('no_wget', dt('wget must be installed to use this command.'));
    $no_wget = TRUE;
  }
  if (!isset($no_fgrep) && !isset($no_wget)) {
    $exec = 'fgrep --recursive VirtualDocumentRoot /etc/apache2';
    if (!drush_shell_exec($exec) || !drush_shell_exec_output()) {
      drush_log(dt('Apache has not been setup with a VirtualDocumentRoot, you will not be able to access the site via a web browser.  Talk to Jake.'), 'warning');
    }
    $destination_domain = drush_get_option('destination-domain');
    $exec = "wget $destination_domain --server-response --output-document=/dev/null 2>&1";
    drush_shell_exec($exec);
    $headers = implode("\n", drush_shell_exec_output());
    if (strpos($headers, 'failed: ') !== FALSE) {
      drush_log(dt('The domain cannot be resolved.  DNS probably needs to be configured for wildcards.  You will not be able to access the site via a web browser.  Talk to Jake.'), 'warning');
    }
    if (strpos($headers, 'WWW-Authenticate: ') === FALSE) {
      $http_auth = <<<EOD
              
        <Directory /home/*/domains>
          AuthName "Authentication Required"
          AuthType Basic
          AuthUserFile /etc/apache2/.htpasswd
          require valid-user
        </Directory>
EOD;
      drush_log(dt('Apache has not been setup with HTTP authentication on this directory.  It will be wide-open to web crawlers and to the public.  You can add something like the following to the Apache config (or talk to Jake): !config', array('!config' => $http_auth)), 'warning');
    }
  }

  if (!drush_get_error()) {
    drush_print('no errors');
  }
}

/**
 * Implements drush_hook_pre_COMMAND().
 */
function drush_advo_site_clone_pre_advo_site_clone() {
  $source_db = drush_get_context('DRUSH_DB_CREDENTIALS');
  drush_print();
  drush_print('---SUMMARY---');
  drush_print(dt("Source path         : @source-path", array('@source-path' => drush_locate_root())));
  drush_print(dt("Source site         : @source-site", array('@source-site' => drush_get_context('DRUSH_DRUPAL_SITE_ROOT'))));
  drush_print(dt("Source DB name      : @source-db-name", array('@source-db-name' => $source_db['name'])));
  drush_print(dt("Destination path    : @destiation-path", array('@destiation-path' => _advo_site_clone_get_destination_path())));
  drush_print(dt("Destination site    : @destiation-site", array('@destiation-site' => 'sites/'. drush_get_option('destination-domain'))));
  drush_print(dt("Destination DB name : @destiation-db-name", array('@destiation-db-name' => drush_get_option('destination-db-name'))));
  drush_print(dt("New Git branch      : @git-branch", array('@git-branch' => drush_get_option('git-branch'))));
  foreach (_advo_site_clone_get_db_credentials() as $option => $value) {
    if ($value) {
      if ($option == 'db-su-pw') {
        $value = '--hidden--';
      }
      $option = str_pad($option, 20);
      drush_print(dt("$option: @value", array('@value' => $value)));
    }
  }
  drush_print();
  if (!drush_confirm(dt('Do you really want to continue?'))) {
    return drush_user_abort();
  }

}

/**
 * Implements drush_COMMAND().
 */
function drush_advo_site_clone() {
  $functions = array(
    '_drush_advo_site_clone_rsync',
    '_drush_advo_site_clone_git_branch',
    '_drush_advo_site_clone_create_site',
    '_drush_advo_site_clone_create_files',
    '_drush_advo_site_clone_copy_db',
    '_drush_advo_site_clone_mysql_info',
  );
  foreach ($functions as $function) {
    if ((!drush_get_error() || drush_get_option('continue-on-error'))) {
      $function();
    }
    else {
      drush_log(dt('Not calling !function due to prior error.', array('!function' => $function)), 'warning');
    }
  }
  
  $errors = drush_get_error();
  drush_log('Complete', $errors ? 'error' : 'success');
  if ($errors) {
    drush_print('with errors, please review above');
  }

}

/**
 * Write the new DB info to ~/mysql.info.
 */
function _drush_advo_site_clone_mysql_info() {
  $path = drush_locate_root();

  // This command is probably being run by a dev who has their own home
  // directory.  So instead we'll traverse up the tree to find the file.
  for ($index = 0; $index < 10; $index++) {
    if (!file_exists($path)) {
      break;
    }
    if (file_exists($path .'/mysql.info')) {
      $mysql_info = realpath($path .'/mysql.info');
      break;
    }
    $path .= '/..';
  }
  if (!isset($mysql_info)) {
    drush_log('No mysql.info found.  Please update manually.', 'warning');
    return;
  }
  $txt = file_get_contents($mysql_info);
  if (!$txt) {
    drush_set_error('mysql_info_cannot_read', 'Cannot read mysql.info.  Please update manually.');
    return;
  }

  $source_db = drush_get_context('DRUSH_DB_CREDENTIALS');
  $pattern = '!^(.*d(?:atabase)?\s*:?\s*)\b(' . preg_quote($source_db['name']) . ')\b(.*)$!m';
  if (!preg_match($pattern, $txt, $matches)) {
    drush_set_error('mysql_info_cannot_find_source', 'Cannot find source DB in mysql.info.  Please update manually.');
    return;
  }
  $new_line = $matches[1] . drush_get_option('destination-db-name') . $matches[3];
  if (!file_put_contents($mysql_info, $txt . "\n" . $new_line . "\n")) {
    drush_set_error('mysql_info_cannot_write', "Cannot write to mysql.info.  Please update manually.\n$new_line");
    return;
  }

  drush_log(dt('Updated mysql.info.'), 'success');
}

/**
 * `drush sql-sync` the DB.
 */
function _drush_advo_site_clone_copy_db() {
  drush_log('Starting DB copy...', 'status');

  $commandline_options = array(
    'create-db' => TRUE,
    'no-cache' => TRUE,
    'no-sanitize' => TRUE,
    'yes' => TRUE,
  );
  // Add DB options.
  foreach (_advo_site_clone_get_db_credentials() as $option => $value) {
    if ($value) {
      $commandline_options[$option] = $value;
    }
  }
  drush_log('commandline_options: ' . var_export($commandline_options, TRUE));

  $commandline_args[] = str_replace('sites/', '', drush_get_context('DRUSH_DRUPAL_SITE_ROOT'));
  $commandline_args[] = drush_get_option('destination-domain');
  drush_log('commandline_args: ' . var_export($commandline_args, TRUE));

  // Temporarily create the sites folder here in the source so that we can read the info.
  $source_site = _advo_site_clone_get_destination_path() . '/sites/' . drush_get_option('destination-domain') . '/';
  $destination_site = drush_locate_root() . '/sites/' . drush_get_option('destination-domain') . '/';

  // Copy sites directory.
  $file_excludes = _advo_site_clone_build_file_excludes();
  $exec = "rsync --archive --copy-dirlinks --delete $file_excludes $source_site $destination_site";
  drush_log($exec);
  $exec_result = drush_op_system($exec);
  if ($exec_result != 0) {
    drush_set_error('DRUSH_RSYNC_FAILED', dt("Could not create temporary sites directory."));
    return;
  }

  // sql-sync.
  // Deal with old versions of drush.
  if (function_exists('drush_invoke_args')) {
    foreach ($commandline_options as $commandline_option => $value) {
      drush_set_option($commandline_option, $value);
    }
    $args = array_merge($commandline_args, $commandline_options);
    drush_log('old version of Drush args: ' . var_export($args, TRUE));
    if (function_exists('module_exists') && module_exists('deploy')) {
      drush_print('You may see several warnings here about a string where arrays are expected caused by Deploy module.  Please ignore.');
    }
    $result = drush_invoke_args('sql-sync', $args);
  }
  else {
    drush_log('drush_invoke_process @self sql-sync');
    $result = drush_invoke_process('@self', 'sql-sync', $commandline_args, $commandline_options);
  }
  if (!$result) {
    drush_set_error('sql-sync_failed', dt("Could copy to new DB."));
    // Don't return so that clean-up can still happen.
  }
  else {
    drush_log(dt('Coppied DB.'), 'success');
  }

  // Clean-up temp sites folder.
  $exec_result = drush_op_system("rm -rf $destination_site");
  if ($exec_result != 0) {
    drush_log(dt("Could not clean-up temporary sites directory !destination_site.", array('!destination_site' => $destination_site)), 'warning');
    return;
  }

}

/**
 * Creates new sites directory.
 */
function _drush_advo_site_clone_create_site() {
  $destination_path = _advo_site_clone_get_destination_path();
  $source_site = $destination_path . '/' . drush_get_context('DRUSH_DRUPAL_SITE_ROOT') . '/';
  $destination_site = $destination_path . '/sites/' . drush_get_option('destination-domain');

  // Copy sites directory.
  $file_excludes = _advo_site_clone_build_file_excludes();
  $exec = "rsync --archive --copy-dirlinks --delete $file_excludes $source_site $destination_site";
  drush_log($exec);
  $exec_result = drush_op_system($exec);
  if ($exec_result != 0) {
    drush_set_error('DRUSH_RSYNC_FAILED', dt("Could not rsync from !source to !dest", array('!source' => $source, '!dest' => $destination)));
    return;
  }
  drush_log(dt('Created sites directory.'), 'success');

  // Update settings.php with new DB name.
  $source_db = drush_get_context('DRUSH_DB_CREDENTIALS');
  $source_db_name = $source_db['name'];
  $destination_db_name = drush_get_option('destination-db-name');

  $settings_php_path = $destination_site . '/settings.php';
  $settings_php = file_get_contents($settings_php_path);
  if (!$settings_php) {
    drush_set_error('no_settings_php', dt("Could not find or open new settings.php file"));
    return;
  }
  if (version_compare(drush_drupal_version(), '7') < 0) {
    // D6 style settings.php.
    $settings_php_new = preg_replace('!([\'"]mysql.+/)(' . preg_quote($source_db_name) . ')([\'"])!', '${1}' . preg_quote($destination_db_name) . '${3}', $settings_php, -1, $count);
  }
  else {
    // D7 style settings.php.
    $settings_php_new = preg_replace('!([\'"]database[\'"]\s*=>\s*[\'"])(' . preg_quote($source_db_name) . ')([\'"])!', '${1}' . preg_quote($destination_db_name) . '${3}', $settings_php, -1, $count);

  }
  if (!$settings_php_new || !$count) {
    // Maybe this is D6?  I wonder if we can find this out more reliably.

    drush_set_error('could_not_replace_db_name', dt("Could not replace the DB name in settings.php."));
    return;
  }
  if (!file_put_contents($settings_php_path, $settings_php_new)) {
    drush_set_error('could_not_write_updated_settings_php', dt("Could not write the updated settings.php file."));
    return;
  }
  drush_log(dt('Updated settings.php.'), 'success');

  // Since this branch site is really only temporary, git ignore the sites dir.
  // For now, skip this step, at least until we have a corresponding drush
  // command to merge things back into master.
  // If we ever do that, change the validation steps so that Git checks are
  // performed even if a new branch is not being created.
//  $git_ignore = $destination_path . '/.gitignore';
//  if (file_exists($git_ignore)) {
//    $append = "\n\n# Ignore temporary site directory.";
//    $append .= "\nsites/" . drush_get_option('destination-domain') . "\n";
//    if (file_put_contents($git_ignore, $append, FILE_APPEND)) {
//      $exec = "git add $git_ignore && git commit -m'Ignored temporary site directory'";
//      if (!drush_shell_cd_and_exec($destination_path, $exec)) {
//        drush_log(dt('Cannot ignore new sites directory in Git.'), 'warning');
//      }
//    }
//    else {
//      drush_log('Could not ignore new sites directory.', 'warning');
//    }
//  }

}

/**
 * Creates the files directory in the new site.
 */
function _drush_advo_site_clone_create_files() {
  $files = _advo_site_clone_get_files_dir();
  $source_files = drush_locate_root() . '/' . $files;
  $destination_files = _advo_site_clone_get_destination_path() . '/' . $files;

  // Create directory.
  if (!file_exists($destination_files)) {
    if (!drush_shell_exec("mkdir $destination_files")) {
      drush_set_error('files_create_failed', dt('Could not create the new files directory @files.', array('@files' => $destination_files)));
    }
  }

  // Match perms of source.
  $perms = substr(decoct(fileperms($source_files)), -4);
  if (!chmod($destination_files, octdec($perms))) {
    drush_set_error('files_chmod_failed', dt('Could not set permissions on the new files directory.'));
  }

  if (!drush_get_error()) {
    drush_log(dt('Created files directory.'), 'success');
  }

}

/**
 * Create a new git Branch.
 */
function _drush_advo_site_clone_git_branch() {
  $branch = drush_get_option('git-branch');
  if (!$branch) {
    drush_log('Not creating a new Git branch.');
    return;
  }

  drush_log('Creating Git branch...', 'status');
  $working_directory = _advo_site_clone_get_destination_path();
  $create_flag = '';
  if (!drush_get_option('git-branch-exists')) {
    $exec = "git checkout -b $branch";
    $exec .= " && git push -u origin $branch";
    if (!drush_shell_cd_and_exec($working_directory, $exec)) {
      drush_set_error('git_branch_failed', dt("Could not create new Git branch."));
      return;
    }
  }
  else {
    $exec = "git checkout --track origin/$branch";
    if (!drush_shell_cd_and_exec($working_directory, $exec)) {
      drush_set_error('git_branch_failed', dt("Could not checkout Git branch."));
      return;
    }
  }
  drush_log(dt('Git branch created.'), 'success');
}

/**
 * Invoke Drush core-rsync.
 */
function _drush_advo_site_clone_rsync() {
  drush_log('Starting rsync...', 'status');

  // We _should_ be using drush_invoke('core-rsync', $arguments) but the exclude
  // paths keep getting FUBARed and I've spent waaaaaay to much time trying to
  // debug.
  $source = drush_locate_root() . '/';
  $destination = _advo_site_clone_get_destination_path();
  $excludes = array();
  if ($files = _advo_site_clone_get_files_dir()) {

  }
  $file_excludes = _advo_site_clone_build_file_excludes();
  // Don't use --delete here because if a sites directory from a prior sync did
  // not have the write bit, rsync won't be able to clean it up.  We already
  // warned the user that the site exists so just carry on. 
  $exec = "rsync --archive --copy-dirlinks $file_excludes $source $destination";
  drush_log($exec);
  $exec_result = drush_op_system($exec);
  if ($exec_result != 0) {
    drush_set_error('DRUSH_RSYNC_FAILED', dt("Could not rsync from !source to !dest", array('!source' => $source, '!dest' => $destination)));
  }
  else {
    drush_log(dt('Rsync was succesfull.'), 'success');
  }
}

/**
 * Build a list of --exclude commands of the files directories suitable for
 * rsync.
 *
 * @return string
 */
function _advo_site_clone_build_file_excludes() {
  $excludes = '';
  foreach (array('public', 'private') as $visibility) {
    if ($files = _advo_site_clone_get_files_dir($visibility)) {
      $excludes .= " --exclude='/$files/'";
    }
  }
  return $excludes;
}

/**
 * What is the path to the files directory.
 *
 * @param $type = 'public'
 *  Either 'public', or 'private'.
 *
 * @return string
 */
function _advo_site_clone_get_files_dir($type = 'public') {
  $status_table = _core_site_status_table(drush_get_option('project',''), drush_get_option('full'));
  $aliases = _core_path_aliases(drush_get_option('project',''));
  if ($type == 'private') {
    return !empty($aliases['%private']) ? $aliases['%private'] : '';
  }
  else {
    return !empty($aliases['%files']) ? $aliases['%files'] : '';
  }
}

/**
 * Dynamically determine the destination directory.
 *
 * @return string
 */
function _advo_site_clone_get_destination_path() {
  if ($desination_path = drush_get_option('destination-path', FALSE)) {
    return $desination_path;
  }
  if ($root = drush_locate_root()) {
    $root = realpath($root);
    // Search for the advo two-level structure.
    $environment_token = '';
    foreach (array('/prod', '/staging', '/dev') as $replace) {
      $root = str_replace($replace, '', $root, $count);
      if ($count) {
        $environment_token = $replace;
        break;
      }
    }
    $root = realpath($root . '/..');
    $desination_path = $root . '/' . drush_get_option('destination-domain') . $environment_token;
    return $desination_path;
  }
  return '';
}

/**
 * Get the db-su and db-su-pw.  Search the filesystem to find it if necessary.
 *
 * @return array()
 *  With keys for 'db-su' and 'db-su-pw'.
 */
function _advo_site_clone_get_db_credentials() {
  static $db_su, $db_su_pw;
  if (!isset($db_su)) {

    $db_su = drush_get_option('db-su', '');
    $db_su_pw = drush_get_option('db-su-pw', '');

    if (!$db_su_pw) {
      // Do we have sudo access?
      if (_advo_site_clone_sudo('fgrep -V')) {
        // Where is the password stored?
        $db_su_pw = _advo_site_clone_sudo("fgrep -m1 password /root/.my.cnf | perl -pw -e's/password=(\w+)/$1/'");
        if (!$db_su_pw) {
          $db_su_pw = _advo_site_clone_sudo("cat /root/pass");
        }
      }

      // If we managed to retreive the password, then db_su must be root.
      if ($db_su_pw && !$db_su) {
        $db_su = 'root';
      }
    }
  }

  return array('db-su' => $db_su, 'db-su-pw' => $db_su_pw);
}

/**
 * Execute a command as sudo.
 *
 * @param string $command
 *  Like 'cat /some/file'.
 * @return string
 *  If sucessful, the output of the command, otherwise a ZLS.
 */
function _advo_site_clone_sudo($command) {
  // Don't use drush_shell_exec() here because we want to see stdout and stderr
  // separately.
  $descriptorspec = array(
    0 => array("pipe", "r"),  // stdin
    1 => array("pipe", "w"),  // stdout
    2 => array("pipe", "w"),  // stderr
  );
  $cmd = "sudo -n $command";
  drush_log("proc_open: $cmd");
  $process = proc_open($cmd, $descriptorspec, $pipes);
  $stdout = stream_get_contents($pipes[1]);
  fclose($pipes[1]);
  drush_log('stdout: ' . var_export($stdout, TRUE));
  $stderr = stream_get_contents($pipes[2]);
  fclose($pipes[2]);
  drush_log('stderr: ' . var_export($stderr, TRUE));
  $exit_code = proc_close($process);
  drush_log('exit code: ' . $exit_code);
  if (!$exit_code && !$stderr) {
    drush_log('Command was successful.');
    return rtrim($stdout, "\n");
  }
  drush_log('Command was unsuccessful.');
  return '';
}
